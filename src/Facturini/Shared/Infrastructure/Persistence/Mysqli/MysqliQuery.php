<?php
/**
 * This software was built by:
 * Daniel Tomé Fernández <danieltomefer@gmail.com>
 * GitHub: danitome24
 */
declare(strict_types=1);

namespace Facturini\Shared\Infrastructure\Persistence\Mysqli;

use Facturini\Shared\Infrastructure\Persistence\Connection;
use Facturini\Shared\Infrastructure\Persistence\Query;
use Facturini\Shared\Infrastructure\Persistence\ResultSet;

final class MysqliQuery implements Query
{
    private $connection;

    private $debugMode;

    public function __construct(Connection $connection, $debugMode = false)
    {
        $this->connection = $connection;
        $this->debugMode = $debugMode;
        $this->query('SET NAMES utf8');
    }

    /**
     * sql_query($query, $id) executes an SQL statement, returns a ResultSet
     * @param $query
     * @return bool|ResultSet
     */
    public function query($query)
    {
        if ($this->debugMode) {
            echo 'SQL query: ' . str_replace(',', ', ', $query) . '<BR>';
        }

        if ($result = mysqli_query($this->connection->connection(), $query)) {
            return new MysqliResultSet($result);
        }

        throw new \Exception(sprintf($result));
    }
}
