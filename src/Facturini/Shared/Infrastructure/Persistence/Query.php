<?php
/**
 * This software was built by:
 * Daniel Tomé Fernández <danieltomefer@gmail.com>
 * GitHub: danitome24
 */

namespace Facturini\Shared\Infrastructure\Persistence;

interface Query
{
    public function query($query);
}
